package ru.diplom.petstore.swagger.io;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class BaseTest {

        //Общая спецификация для создания запроса
        public RequestSpecification basicRequestSpecification(){
            RequestSpecBuilder requestSpec = new RequestSpecBuilder()
                    //.log(LogDetail.ALL)
                    .setBaseUri("https://petstore.swagger.io/v2")
                    .addHeader("access-control-allow-headers","Content-Type,api_key,Authorization")
                    .addHeader("access-control-allow-methods", "GET,POST,DELETE,PUT")
                    .addHeader("access-control-allow-origin","*")
                    .addHeader("content-type", "application/json");
//                .log(LogDetail.ALL);

            return requestSpec.build();
        }

}
