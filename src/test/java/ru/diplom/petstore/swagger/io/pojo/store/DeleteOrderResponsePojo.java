package ru.diplom.petstore.swagger.io.pojo.store;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeleteOrderResponsePojo {
    public int code;
    public String type;
    public String message;
}