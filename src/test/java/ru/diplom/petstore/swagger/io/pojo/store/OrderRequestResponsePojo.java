package ru.diplom.petstore.swagger.io.pojo.store;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequestResponsePojo {
    public int id;
    public int petId;
    public int quantity;
    public Date shipDate;
    public String status;
    public boolean complete;
}