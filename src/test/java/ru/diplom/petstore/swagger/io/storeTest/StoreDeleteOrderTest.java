package ru.diplom.petstore.swagger.io.storeTest;

import io.restassured.RestAssured;
import jdk.jfr.Description;
import org.hamcrest.Matchers;
import org.jetbrains.annotations.Async;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.diplom.petstore.swagger.io.pojo.store.DeleteOrderResponsePojo;
import ru.diplom.petstore.swagger.io.BaseTest;

public class StoreDeleteOrderTest extends BaseTest {

    @DataProvider(name = "deleteDataOrderPositive")
    public Object[] deleteDataOrderPositive() {
        return new Object[]{
                1,8,10
        };
    }

    @DataProvider(name = "deleteDataOrderNegative")
    public Object[] deleteDataOrderNegative() {
        return new Object[]{
                -1,0
        };
    }

    @DataProvider(name = "deleteDataOrderInvalidOrderId")
    public Object[] deleteDataOrderInvalidOrderId() {
        return new Object[]{
                1.5, -1.5, "ds", "ыв"
        };
    }

    //Параметризованный позитивный тест на удаление заказа
    @Test(dataProvider = "deleteDataOrderPositive")
    @io.qameta.allure.Description("Проверка позитивного сценария удаления заказа метод delete store/order/{orderId} ({orderId} >0)")
    public void deleteOrderTestPositive(int orderId) {

        RestAssured
                .given() //предусловие
                .spec(basicRequestSpecification())
                .pathParams("orderId",orderId)
                .when() //действие (условие)
                .delete("store/order/{orderId}")
                .then() // ожидаемый результат после действия
                .statusCode(200)
                .body("code", Matchers.equalTo(200))
                .body("message", Matchers.notNullValue())
                .extract().as(DeleteOrderResponsePojo.class);

    }

    //Параметризованный негативный тест на удаление заказа
    /**Данный тест иногда нужно запускать несколько раз, т.к. не всегда в приложении есть заказы по номерам указанным в доке **/
    @Test(dataProvider = "deleteDataOrderNegative")
    @io.qameta.allure.Description("Проверка позитивного сценария удаления заказа метод delete store/order/{orderId} {orderId} <=0")
    public void deleteOrderTestNegative(int orderId) {

        RestAssured
                .given() //предусловие
                .spec(basicRequestSpecification())
                .pathParams("orderId",orderId)
                .when() //действие (условие)
                .delete("store/order/{orderId}")
                .then() // ожидаемый результат после действия
                .statusCode(404)
                .body("code", Matchers.equalTo(404))
                .body("message", Matchers.equalTo("Order Not Found"))
                .extract().as(DeleteOrderResponsePojo.class);

    }

    // Параметризованный негативный тест на удаление заказа с невалидными orderID
    @Test(dataProvider = "deleteDataOrderInvalidOrderId")
    @Description("Проверка позитивного сценария удаления заказа метод delete store/order/{orderId} {orderId} == невалидные значения")
    @io.qameta.allure.Description("Проверка позитивного сценария удаления заказа метод delete store/order/{orderId} {orderId} == невалидные значени")
    public void deleteOrderTestInvalidOrderID(Object orderId) {

        RestAssured
                .given() //предусловие
                .spec(basicRequestSpecification())
                .pathParams("orderId",orderId)
                .when() //действие (условие)
                .delete("store/order/{orderId}")
                .then() // ожидаемый результат после действия
                .statusCode(404)
                .body("code", Matchers.equalTo(404))
                .body("message", Matchers.notNullValue())
                .extract().as(DeleteOrderResponsePojo.class);

    }

}
