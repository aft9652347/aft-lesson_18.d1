package ru.diplom.petstore.swagger.io.storeTest;

import io.restassured.RestAssured;
import jdk.jfr.Description;
import org.testng.annotations.Test;
import ru.diplom.petstore.swagger.io.BaseTest;

public class StoreGetInventoreTest extends BaseTest {

    @Test
    @io.qameta.allure.Description("Проверка работы метода get store/inventory")
    public void getInventoreTest(){

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .when()
                .get("store/inventory")
                .then()
                .log().all()
                .statusCode(200);

    }


}
