package ru.diplom.petstore.swagger.io.storeTest;

import io.restassured.RestAssured;
import jdk.jfr.Description;
import org.hamcrest.Matchers;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.diplom.petstore.swagger.io.BaseTest;

public class StoreGetOrderTest extends BaseTest {

    @DataProvider(name = "getDataOrderPositive")
    public Object[] getDataOrderPositive() {
        return new Object[]{
               1,2,9,10
        };
    }

    @DataProvider(name = "getDataOrderNegative")
    public Object[] getDataOrderNegative() {
        return new Object[]{
                -90,-1,0,11,66
        };
    }

    @DataProvider(name = "getDataOrderInvalidOrderId")
    public Object[] getDataOrderInvalidOrderId() {
        return new Object[]{
                -1.5,0.80,"SOS","ИД"
        };
    }

    //Параметризованный позитивный тест на получение заказа
    @Test(dataProvider = "getDataOrderPositive")
    @io.qameta.allure.Description("Проверка позитивного сценария получение заказа метод get store/order/{orderId} (1 =< {orderId} <= 10)")
    public void getOrderTest(int orderId) {

        RestAssured
                .given() //предусловие
                .spec(basicRequestSpecification())
                .pathParams("orderId",orderId)
                .when() //действие (условие)
                .get("store/order/{orderId}")
                .then() // ожидаемый результат после действия
                //.log().all()
                .statusCode(200)
                .body("id", Matchers.notNullValue())
                .body("shipDate", Matchers.notNullValue());
        /**Дефект в логике, если заказа нет то стоит это обрабатывать и возвращать ответ 200 с сообщением об отсутсвии заказа
         * по указанному orderId **/

    }

    //Параметризованный негативный тест на получение заказа
    @Test(dataProvider = "getDataOrderNegative")
    @io.qameta.allure.Description("Проверка получения заказа через метод get store/order/{orderId} (1 > {orderId} > 10)")
    public void createOrderNegativeTest(int id) {

        RestAssured
                .given() //предусловие
                .spec(basicRequestSpecification())
                .pathParams("orderId",id)
                .when() //действие (условие)
                .get("store/order/{orderId}")
                .then() // ожидаемый результат после действия
                //.log().all()
                .statusCode(404)
                .body("type", Matchers.equalTo("error"))
                .body("message", Matchers.equalTo("Order not found"));
    }

    //Не параметризованный негативный кейс на получение заказа orderId - дробное значение
    @Test(dataProvider = "getDataOrderInvalidOrderId")
    @io.qameta.allure.Description("Проверка получения заказа через метод get store/order/{orderId} {orderId}==Double")
    public void getOrderNegativeTestIdDouble(Object orderId) {

        RestAssured
                .given() //предусловие
                .spec(basicRequestSpecification())
                .pathParams("orderId",orderId)
                .when() //действие (условие)
                .get("store/order/{orderId}")
                .then() // ожидаемый результат после действия
                //.log().all()
                .statusCode(404)
                .body("code", Matchers.equalTo(404))
                .body("message", Matchers.notNullValue());
    }


    /**TO DO с проверкой тела в зависимости от statusCode
    //Параметризованный позитивный тест на получение заказа
    @Test(dataProvider = "getDataOrderPositive")
    @Description("Проверка позитивного сценария получение заказа метод create store/order/{orderId}  (1 =< {orderId} <= 10)")
    public void getOrderTestXXX(int orderId) {

        ResponseBodyExtractionOptions responsBody = RestAssured
                .given() //предусловие
                .spec(basicRequestSpecification())
                .pathParams("orderId", orderId)
                .when() //действие (условие)
                .get("store/order/{orderId}")
                .then() // ожидаемый результат после действия
                .extract().response();




    }
    **/



}
