package ru.diplom.petstore.swagger.io.storeTest;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import jdk.jfr.Description;
import org.hamcrest.Matchers;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.diplom.petstore.swagger.io.BaseTest;
import ru.diplom.petstore.swagger.io.pojo.store.OrderRequestResponsePojo;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class StorePostOrderTest extends BaseTest {
    Faker faker = new Faker();

    @DataProvider(name = "createDataOrder")
    public Object[][] createDataOrder() {
        return new Object[][]{
                {faker.idNumber().hashCode(), faker.number().numberBetween(1, 500), faker.number().numberBetween(1, 100), faker.date().future(5000, TimeUnit.MICROSECONDS), "placed", true},
                {faker.number().randomDigitNotZero(), faker.number().numberBetween(1, 500), faker.number().numberBetween(1, 100), faker.date().future(5000, TimeUnit.MICROSECONDS), "placed", true},
                {faker.idNumber().hashCode(), faker.number().numberBetween(1, 500), faker.number().numberBetween(1, 100), null, "placed", true},
                {faker.idNumber().hashCode(), faker.number().numberBetween(1, 500), faker.number().numberBetween(1, 100), faker.date().future(5000, TimeUnit.MICROSECONDS), "placed", false},
                {faker.idNumber().hashCode(), faker.number().numberBetween(1, 500), faker.number().numberBetween(1, 100), faker.date().future(5000, TimeUnit.MICROSECONDS), null, true},

        };
    }

    //Параметризованный проверка тела ответа на наличие обязательных полей
    @Test(dataProvider = "createDataOrder")
    @io.qameta.allure.Description("Проверка создания заказа метод create")
    public void createOrderTest(int id, int petId, int quantity, Date shipDate, String status, boolean complete) {

        OrderRequestResponsePojo orderRequestResponsePojo =
                new OrderRequestResponsePojo(id, petId, quantity, shipDate, status, complete);

        RestAssured
                .given() //предусловие
                .spec(basicRequestSpecification())
                .body(orderRequestResponsePojo)
                .when() //действие (условие)
                .post("store/order")
                .then() // ожидаемый результат после действия
                .statusCode(200)
                .body("id", Matchers.notNullValue());

    }

    //Параметризованный кейс по созданию заказа
    @Test(dataProvider = "createDataOrder")
    @io.qameta.allure.Description("Проверка тела ответа на метод create, на возврат всех обязательных полей")
    public void createOrderTestResponseBody(int id, int petId, int quantity, Date shipDate, String status, boolean complete) {

        //SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
        //Date date = new Date(1212121212121L);

        OrderRequestResponsePojo orderRequestResponsePojo =
                new OrderRequestResponsePojo(id, petId, quantity, shipDate, status, complete);

        RestAssured
                .given() //предусловие
                .spec(basicRequestSpecification())
                .body(orderRequestResponsePojo)
                .when() //действие (условие)
                .post("store/order")
                .then() // ожидаемый результат после действия
                .statusCode(200)
                .extract().as(OrderRequestResponsePojo.class);
    }


    //Кейс по созданию заказа с пустым телом запроса
    @Test()
    @io.qameta.allure.Description("Проверка создания заказа с пустым телом запроса, метод create")
    public void createOrderTestResponseBodyEmpty() {

//        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
//        Date date = new Date(112121212121L);
//        System.out.println(date);
//        System.out.println(formater);

        String orderBodyEmpty = "{}";

        //ResponseBodyExtractionOptions responseBody =
                RestAssured
                .given() //предусловие
                .spec(basicRequestSpecification())
                .body(orderBodyEmpty)
                .when() //действие (условие)
                .post("store/order")
                .then() // ожидаемый результат после действия
                .statusCode(200)
                .body("id", Matchers.notNullValue())
                .body("petId", Matchers.equalTo(0))
                .body("quantity", Matchers.equalTo(0))
                .body("complete",Matchers.equalTo(false));
//                .extract().body();
//
//        String petId = responseBody.jsonPath().getString("petId");
//        String quantity = responseBody.jsonPath().getString("quantity");
//        String complete = responseBody.jsonPath().getString("complete");
//        assertEquals(petId, "0");
//        assertEquals(quantity, "0");
//        assertEquals(complete, "false");
    }

}

