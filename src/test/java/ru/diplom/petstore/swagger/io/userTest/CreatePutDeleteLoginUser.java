package ru.diplom.petstore.swagger.io.userTest;

import io.restassured.RestAssured;
import jdk.jfr.Description;
import org.hamcrest.Matchers;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.diplom.petstore.swagger.io.BaseTest;
import ru.diplom.petstore.swagger.io.pojo.user.RequestUserCreatePojo;

public class CreatePutDeleteLoginUser extends BaseTest {

    @DataProvider(name = "dataUser")
    public Object[][] dataUser() {
        return new Object[][]{

                {11010, "Saimon", "Mailown", "Simpson", "SMS@si.com", "lopnilom", "+79548652255", 0}

        };
    }

        @DataProvider(name = "negativeDataUser")
        public Object[][] negativeDataUser(){
            return new Object[][]{

                    {21354,"SOS", "Noy", "Gay", "SMS@si.com", "LolliPop", "+79548652255",0}

            };
    }

    //Позитивный сценарий создания пользователя POST CreateUser
    @Test(dataProvider = "dataUser", priority=1)
    @io.qameta.allure.Description("Создание пользователя, позитивный сценарий")
    public void createUserPositiveTest(int id, String username, String firstName, String lastName,
                                       String email, String password, String phone, int userStatus){
        RequestUserCreatePojo userCreatePojo = new RequestUserCreatePojo(id,username,firstName,lastName,email,password,phone,userStatus);

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .body(userCreatePojo)
                .log().all()
                .when()
                .post("/user")
                .then()
                .statusCode(200)
                .body("code", Matchers.equalTo(200))
                .body("message", Matchers.notNullValue());

    }


    //Позитивный сценарий входа get login
    @Test(dataProvider = "dataUser", priority=2)
    @io.qameta.allure.Description("Залогиниться пользователем (GET login), позитивный сценарий")
    public void loginUserPositiveTest(int id, String username, String firstName, String lastName,
                                       String email, String password, String phone, int userStatus){

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .param(username, password)
                .when()
                .get("/user/login")
                .then()
                .statusCode(200)
                .body("code", Matchers.equalTo(200))
                .body("message", Matchers.notNullValue());

    }

    //Позитивный сценарий выхода GET logout
    @Test(priority=3)
    @io.qameta.allure.Description("Релогин пользователя (GET logout), позитивный сценарий")
    public void logoutUserPositiveTest(){

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .log().all()
                .when()
                .get("/user/logout")
                .then()
                .log().all()
                .statusCode(200)
                .body("code", Matchers.equalTo(200))
                .body("message", Matchers.notNullValue());

    }

    //Позитивный сценарий редактирования пользователя PUT user
    @Test(dataProvider = "dataUser", priority=4)
    @io.qameta.allure.Description("Обновить данные о пользователе по username")
    public void putUserPositiveTest(int id, String username, String firstName, String lastName,
                                       String email, String password, String phone, int userStatus){

        RequestUserCreatePojo userCreatePojo = new RequestUserCreatePojo(
                id,"Djoe","firstName","Kabuki",email,password,phone,userStatus);

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .pathParams("username", username)
                .body(userCreatePojo)
                .log().all()
                .when()
                .put("/user/{username}")
                .then()
                .log().all()
                .statusCode(200)
                .body("code", Matchers.equalTo(200))
                .body("message", Matchers.notNullValue());

    }

    //Позитивный сценарий получения пользователя по username
    @Test(priority=5)
    @io.qameta.allure.Description("Удалить пользователя (DELETE user) ")
    public void deleteUserPositiveTest(){

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .pathParams("username", "Djoe")
                .log().all()
                .when()
                .delete("/user/{username}")
                .then()
                .log().all()
                .statusCode(200);
    }

    //Позитивный сценарий получения пользователя по username
    @Test(priority=6)
    @io.qameta.allure.Description("Удалить пользователя (DELETE user) ")
    public void deleteUserNegativeTest(){

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .pathParams("username", "Djoe")
                .log().all()
                .when()
                .delete("/user/{username}")
                .then()
                .log().all()
                .statusCode(404);
    }


    /**В целом, следующий кейс планировался как негативный, но как оказалось им в принципе всё ровно кого переделовать,
     *  существующего пользака или так, первого встречного. В первом варианте использовал данные из 6 кейса (удалённого пользака)**/

    //Позитивный сценарий редактирования пользователя PUT user
    @Test(dataProvider = "negativeDataUser", priority=7)
    @io.qameta.allure.Description("Обновить данные о пользователе по username")
    public void putUserNegativeTest(int id, String username, String firstName, String lastName,
                                    String email, String password, String phone, int userStatus){

        RequestUserCreatePojo userCreatePojo = new RequestUserCreatePojo(
                id,"use","firstName","Kabuki",email,password,phone,userStatus);

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .pathParams("username", username)
                .body(userCreatePojo)
                //.log().all()
                .when()
                .put("/user/{username}")
                .then()
                //.log().all()
                .statusCode(404)
                .body("message", Matchers.notNullValue());

    }

    /**КАк оказалось логинят они тоже кого не поподя))**/
    //Позитивный сценарий входа get login
    @Test(dataProvider = "dataUser", priority=8)
    @io.qameta.allure.Description("Залогиниться пользователем (GET login), позитивный сценарий")
    public void loginUserNegativeTest(int id, String username, String firstName, String lastName,
                                      String email, String password, String phone, int userStatus){

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .param(username, password)
                .when()
                .get("/user/login")
                .then()
                .statusCode(404)
                .body("message", Matchers.notNullValue());

    }




}
