package ru.diplom.petstore.swagger.io.userTest;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import jdk.jfr.Description;
import org.hamcrest.Matchers;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.diplom.petstore.swagger.io.BaseTest;
import ru.diplom.petstore.swagger.io.pojo.user.RequestUserCreatePojo;

public class CreateUserTest extends BaseTest {

    Faker faker = new Faker();


    @DataProvider(name = "positiveDataCreateUser")
    public Object[][] positiveDataCreateUser(){
        return new Object[][]{
                {faker.idNumber().hashCode(),faker.name().name(), faker.name().firstName(), faker.name().lastName(),
                 faker.internet().emailAddress(), faker.internet().password(), faker.phoneNumber().phoneNumber(),faker.number().randomDigitNotZero()},
                {11,"Кунь", "Ван-чу", "Сой", "SVJ@mail.ru", "www222", "89565145858",11} //ввод кириллицы
        };
    }

    @DataProvider(name = "negativeDataCreateUser")
    public Object[][] negativeDataCreateUser(){
        return new Object[][]{
                {0,"", "", "", "", "", "",0},
                {-1,null, null, null, null, null, null,-0},

        };
    }

    @DataProvider(name = "negativeDataCreateIdenticalUser")
    public Object[][] negativeDataCreateIdenticalUser(){
        return new Object[][]{
                //данный пользователь ранее создан в позитивной проверке создания пользователя
                {11,"Кунь", "Ван-чу", "Сой", "SVJ@mail.ru", "www222", "89565145858",11}

        };
    }

    @DataProvider(name = "invalidDataCreateUser")
    public Object[] invalidDataCreateUser(){
        return new Object[]{
                "null", ""
        };
    }

    //Позитивный сценарий создания пользователя
    @Test(dataProvider = "positiveDataCreateUser")
    @io.qameta.allure.Description("Создание пользователя, позитивный сценарий")
    public void createUserPositiveTest(int id, String username, String firstName, String lastName,
                                       String email, String password, String phone, int userStatus){
        RequestUserCreatePojo userCreatePojo = new RequestUserCreatePojo(id,username,firstName,lastName,email,password,phone,userStatus);

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .body(userCreatePojo)
                //.log().all()
                .when()
                .post("/user")
                .then()
                .statusCode(200)
                .body("code", Matchers.equalTo(200))
                .body("message", Matchers.notNullValue());

    }

    //Сценарий создания пользователя с невалидными значениями полей
    /**Вообще сценарий подразумевался как негативный, но в доке скачано: "по умолчания успешная работа"**/
    @Test(dataProvider = "negativeDataCreateUser")
    @io.qameta.allure.Description("Создание пользователя, как оказалось позитивный сценарий")
    public void createUserNegativeTest(int id, String username, String firstName, String lastName,
                                       String email, String password, String phone, int userStatus){

        RequestUserCreatePojo userCreatePojo = new RequestUserCreatePojo(id,username,firstName,lastName,email,password,phone,userStatus);

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .body(userCreatePojo)
                .when()
                .post("/user")
                .then()
                .statusCode(200)
                .body("code", Matchers.equalTo(200))
                .body("message", Matchers.notNullValue());

    }

    //Сценарий создания двух одинаковых пользователей
    /**Будем считать что в логике допущен дефект, ведь нельзя создать в системе два одинаковых пользователя
     * предположим, что в данном случае должно быть сообщение - "Такой пользователь уже существует""**/
    @Test(dataProvider = "negativeDataCreateIdenticalUser")
    @io.qameta.allure.Description("Создание пользователя, как оказалось позитивный сценарий")
    public void createIdenticalUserNegativeTest(int id, String username, String firstName, String lastName,
                                       String email, String password, String phone, int userStatus){

        RequestUserCreatePojo userCreatePojo = new RequestUserCreatePojo(id,username,firstName,lastName,email,password,phone,userStatus);

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .body(userCreatePojo)
                .when()
                .post("/user")
                .then()
                .statusCode(200)
                .body("code", Matchers.equalTo(200))
                .body("message", Matchers.equalTo("Такой пользователь уже существует"));

    }

    //Сценарий проверяющий работу метода при передаче навалидного тела запроса
    @Test(dataProvider = "invalidDataCreateUser")
    @io.qameta.allure.Description("Попытка создание пользователя с невалидным телом запроса")
    public void createUserInvalidDataTest(String body){

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .body(body) //передаём сразу в тело запроса и параметра теста
                .when()
                .post("/user")
                .then()
                .statusCode(405);
    }

}
