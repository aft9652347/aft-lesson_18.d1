package ru.diplom.petstore.swagger.io.userTest;

import io.restassured.RestAssured;
import jdk.jfr.Description;
import org.hamcrest.Matchers;
import org.testng.annotations.Test;
import ru.diplom.petstore.swagger.io.BaseTest;

public class LogoutUserTest extends BaseTest {

    @Test
    @io.qameta.allure.Description("Проверка работы метода logout")
    public void logoutUserTest(){

        RestAssured
                .given()
                .spec(basicRequestSpecification())
                .when()
                .get("user/logout")
                .then()
                //.log().all()
                .statusCode(200)
                .body("code", Matchers.equalTo(200))
                .body("message", Matchers.equalTo("ok"));


    }
}
